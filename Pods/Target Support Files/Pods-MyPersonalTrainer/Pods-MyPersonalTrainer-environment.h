
// To check if a library is compiled with CocoaPods you
// can use the `COCOAPODS` macro definition which is
// defined in the xcconfigs so it is available in
// headers also when they are imported in the client
// project.


// Alamofire
#define COCOAPODS_POD_AVAILABLE_Alamofire
#define COCOAPODS_VERSION_MAJOR_Alamofire 1
#define COCOAPODS_VERSION_MINOR_Alamofire 2
#define COCOAPODS_VERSION_PATCH_Alamofire 2

// SWRevealViewController
#define COCOAPODS_POD_AVAILABLE_SWRevealViewController
#define COCOAPODS_VERSION_MAJOR_SWRevealViewController 2
#define COCOAPODS_VERSION_MINOR_SWRevealViewController 3
#define COCOAPODS_VERSION_PATCH_SWRevealViewController 0

// SwiftOverlays
#define COCOAPODS_POD_AVAILABLE_SwiftOverlays
#define COCOAPODS_VERSION_MAJOR_SwiftOverlays 0
#define COCOAPODS_VERSION_MINOR_SwiftOverlays 11
#define COCOAPODS_VERSION_PATCH_SwiftOverlays 0

// SwiftyJSON
#define COCOAPODS_POD_AVAILABLE_SwiftyJSON
#define COCOAPODS_VERSION_MAJOR_SwiftyJSON 2
#define COCOAPODS_VERSION_MINOR_SwiftyJSON 2
#define COCOAPODS_VERSION_PATCH_SwiftyJSON 0

