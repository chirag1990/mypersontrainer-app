//
//  BackTableViewController.swift
//  MyPersonalTrainer
//
//  Created by Chirag Tailor on 25/05/2015.
//  Copyright (c) 2015 TCT. All rights reserved.
//

import UIKit


class BackTableViewController: UITableViewController{
    
    var TableArray = [String]()
    var TableImage = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        TableArray = ["Home","Profile","About"]
        TableImage = ["home.png", "Profile.png", "info.png"]
        
    }
    
    override func   didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return TableArray.count
    }
    
    //Below function allows you to define each cell
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell{
        
        let cell = tableView.dequeueReusableCellWithIdentifier(TableArray[indexPath.row], forIndexPath: indexPath) as! MenuCell
    

        cell.MenuLabel.text? = TableArray[indexPath.row]
        cell.MenuImage.image = UIImage(named: TableImage[indexPath.row])
        
        return cell
    }    

    
}