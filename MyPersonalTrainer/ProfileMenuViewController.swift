//
//  ProfileMenuViewController.swift
//  MyPersonalTrainer
//
//  Created by Chirag Tailor on 26/05/2015.
//  Copyright (c) 2015 TCT. All rights reserved.
//

import UIKit
import SwiftOverlays
import SwiftyJSON

class ProfileMenuViewController : UIViewController {
    @IBOutlet weak var Open: UIBarButtonItem!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var userFirstName: UILabel!
    @IBOutlet weak var userLastName: UILabel!
    
    override func viewDidLoad() {
        Open.target = self.revealViewController()
        Open.action = Selector("revealToggle:")
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        self.getUserDetails()
    }
    
    // Getting user details from the API
    func getUserDetails(){
        
        if Reachability.isConnectedToNetwork() == true
        {
            let url = "user/get/"+user["id"]!
            let method = "GET"
            let parameters = [ // this sould be empty to be honest
                "email": "",
            ]
            
            var ConnectionMGR: ConnectionManager = ConnectionManager();
            ConnectionMGR.doRequest(url, method: method, params: parameters, file: "false")
                {
                    (result: JSON) in
                    var success = result["success"].stringValue
                    var message = result["message"].stringValue
                    
                    if(success == "1"){
                        
                        var loggedinUser = apiResponse["user"]!.dictionaryValue;
                        var first_name = loggedinUser["first_name"]!.stringValue;
                        var last_name = loggedinUser["last_name"]!.stringValue;
                        
                        self.userFirstName.text = first_name
                        self.userLastName.text = last_name
                        
                        println(apiResponse);
                    }
                    else{
                        
                    }
            }
        }
        else
        {
            let alert = UIAlertView()
            alert.title = "Connection Error!"
            alert.message = "Failed to get data from the api please make sure you are connected to internet"
            alert.addButtonWithTitle("ok")
            alert.show()
            self.removeAllOverlays()
        }
        
    }
}