//
//  ConnectionManager.swift
//  ApiExample
//
//  Created by Nitin Vaja on 13/05/2015.
//  Copyright (c) 2015 Rain Cloud Limited. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

var apiResponse = Dictionary<String, JSON>();

class ConnectionManager{
    
    let user = "personaltrainer"
    let password = "52454575khjhew5584dehhajsdjee5d75sw"
    let endpoint = "http://mypersonaltrainer.rain-cloud.co.uk/api/v1/"
    
    
    func doRequest(url:String, method:String, params:NSDictionary, file:String, completion: (result: JSON) -> Void)
    {
        var jsonResult: NSDictionary = NSDictionary();
        
        let plainString = "\(user):\(password)" as NSString
        let plainData = plainString.dataUsingEncoding(NSUTF8StringEncoding)
        let base64String = plainData?.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0))
        
        Alamofire.Manager.sharedInstance.session.configuration.HTTPAdditionalHeaders = ["Authorization": "Basic " + base64String!]
        
        if(method == "POST")
        {
            Alamofire.request(.POST, endpoint+url, parameters: params as? [String : String])
                .responseJSON { (req, res, json, error) in
                    if(error != nil)
                    {
                        let alert = UIAlertView()
                        alert.title = "Connection Error!"
                        alert.message = "Failed to get data from the api please make sure you are connected to internet"
                        alert.addButtonWithTitle("ok")
                        alert.show()
                    }
                    else
                    {
                        var json = JSON(json!)
                        completion(result: json)
                    }
            }
        }
        
        if(method == "GET")
        {
            Alamofire.request(.GET, endpoint+url, parameters: params as? [String : String])
                .responseJSON { (req, res, json, error) in
                    if(error != nil)
                    {
                        let alert = UIAlertView()
                        alert.title = "Connection Error!"
                        alert.message = "Failed to get data from the api please make sure you are connected to internet"
                        alert.addButtonWithTitle("ok")
                        alert.show()
                    }
                    else
                    {
                        var json = JSON(json!)
                        completion(result: json)
                    }
            }
            
        }

    }
}