//
//  ViewController.swift
//  MyPersonalTrainer
//
//  Created by Chirag Tailor on 24/05/2015.
//  Copyright (c) 2015 TCT. All rights reserved.
//

import UIKit
import SwiftOverlays
import SwiftyJSON


class RegisterViewController: UIViewController {
    
    var type = "personal_trainer";
    
    @IBOutlet weak var checkBoxPersonalTrainer: UIButton!
    @IBOutlet weak var checkBoxStandard: UIButton!
    
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    
    @IBOutlet weak var emailTextField: UITextField!
    
    @IBOutlet weak var passwordTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        dispatch_async(dispatch_get_main_queue(), { () -> Void in
            
            let text = "Please wait..."
            self.showWaitOverlayWithText(text)

        });

        
        // Setting default to I am a personal Trainer
        checkBoxStandard.hidden = true;
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        //test comments
    }
    
    // changing the bar colour to white
    override func preferredStatusBarStyle() -> UIStatusBarStyle
    {
        return UIStatusBarStyle.LightContent
    }
    
    @IBAction func btnPersonalTrainer(sender: AnyObject) {
        
        type = "personal_trainer";
        checkBoxPersonalTrainer.hidden = false;
        checkBoxStandard.hidden = true;
    }
    
    @IBAction func btnstandardUser(sender: AnyObject) {
        
        type = "standard";
        checkBoxPersonalTrainer.hidden = true;
        checkBoxStandard.hidden = false;        
    }
    
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        if let touch = touches.first as? UITouch {
            self.view.endEditing(true)
        }
        super.touchesBegan(touches , withEvent:event)
    }
    
    @IBAction func btnSignUp(sender: AnyObject) {
    
        let text = "Creating new account...."
        showWaitOverlayWithText(text)
        
        if Reachability.isConnectedToNetwork() == true
        {
            let url = "user/register"
            let method = "POST"
            let parameters = [
                "first_name": firstNameTextField.text,
                "last_name": lastNameTextField.text,
                "type": type,
                "email": emailTextField.text,
                "password": passwordTextField.text,
            ]
            
            var ConnectionMGR: ConnectionManager = ConnectionManager();
            ConnectionMGR.doRequest(url, method: method, params: parameters, file: "false")
            {
                (result: JSON) in
                var success = result["success"].stringValue
                var message = result["message"].stringValue
                
                if(success == "1"){
                    apiResponse = result["data"].dictionaryValue
                    
                    var loggedinUser = apiResponse["user"]!.dictionaryValue;
                    
                    user["first_name"] = loggedinUser["first_name"]!.stringValue;
                    user["last_name"] = loggedinUser["last_name"]!.stringValue
                    user["id"] = loggedinUser["id"]!.stringValue
                    user["email"] = loggedinUser["email"]!.stringValue
                    user["type"] = loggedinUser["type"]!.stringValue
                    
                    self.removeAllOverlays()
                    
                    var usr: User = User();
                    usr.deleteData()
                    usr.createNewUser(user,password: self.passwordTextField.text);
                    
                    dispatch_async(dispatch_get_main_queue()) {
                        self.performSegueWithIdentifier("home", sender: self)
                    }
                }
                else {
                    let alert = UIAlertView()
                    alert.title = "Login error!"
                    alert.message = message
                    alert.addButtonWithTitle("ok")
                    alert.show()
                    self.removeAllOverlays()
                    
                }
            }
        }
        else
        {
            let alert = UIAlertView()
            alert.title = "Connection Error!"
            alert.message = "Failed to get data from the api please make sure you are connected to internet"
            alert.addButtonWithTitle("ok")
            alert.show()
            self.removeAllOverlays()
        }
        
    }
    
}

