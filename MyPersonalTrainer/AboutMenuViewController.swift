//
//  AboutMenuViewController.swift
//  MyPersonalTrainer
//
//  Created by Chirag Tailor on 26/05/2015.
//  Copyright (c) 2015 TCT. All rights reserved.
//

import UIKit

class AboutMenuViewController : UIViewController {
    
    @IBOutlet weak var Open: UIBarButtonItem!
    override func viewDidLoad() {
        Open.target = self.revealViewController()
        Open.action = Selector("revealToggle:")
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
    }
}