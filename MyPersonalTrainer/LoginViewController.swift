//
//  ViewController.swift
//  MyPersonalTrainer
//
//  Created by Chirag Tailor on 24/05/2015.
//  Copyright (c) 2015 TCT. All rights reserved.
//

import UIKit
import SwiftOverlays
import SwiftyJSON


// Global Variable to Store User Details
var user = [
    "first_name" : "",
    "last_name" : "",
    "type" : "",
    "email" : "",
    "id" : "",
];


class LoginViewController: UIViewController {
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Checking to see if this use has already logged in before
        // so we check in the databse if the user has already logged in before if so get the details from the database and log user in.
        var usr: User = User();
        var result = usr.getUser();
        
        if(result.count > 0){
            var email = "";
            var password = "";
            for items in result{
                email = items.valueForKey("email") as! String
                password = items.valueForKey("password") as! String
            }
            
            doLogin(email, password: password)
        }

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        //test comments
    }

    // changing the bar colour to white
    override func preferredStatusBarStyle() -> UIStatusBarStyle
    {
        return UIStatusBarStyle.LightContent
    }
    
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        if let touch = touches.first as? UITouch {
            self.view.endEditing(true)
        }
        super.touchesBegan(touches , withEvent:event)
    }
    
    // Logs User In
    @IBAction func btnLogin(sender: AnyObject) {
        var email = emailTextField.text
        var password = passwordTextField.text
        doLogin(email, password:password);
    }
    
    func doLogin(email:String, password:String)
    {
        let text = "Loggin in..."
        showWaitOverlayWithText(text)
    
        if Reachability.isConnectedToNetwork() == true
        {
            let url = "user/login"
            let method = "POST"
            let parameters = [
                "email": email,
                "password": password,
            ]
            var ConnectionMGR: ConnectionManager = ConnectionManager();
            ConnectionMGR.doRequest(url, method: method, params: parameters, file: "false")
            {
                (result: JSON) in
                var success = result["success"].stringValue
                var message = result["message"].stringValue
                
                if(success == "1"){
                    apiResponse = result["data"].dictionaryValue
                    
                    var loggedinUser = apiResponse["user"]!.dictionaryValue;
                    
                    user["first_name"] = loggedinUser["first_name"]!.stringValue;
                    user["last_name"] = loggedinUser["last_name"]!.stringValue
                    user["id"] = loggedinUser["id"]!.stringValue
                    user["email"] = loggedinUser["email"]!.stringValue
                    user["type"] = loggedinUser["type"]!.stringValue
                    
                    var usr: User = User();
                    usr.deleteData()
                    usr.createNewUser(user,password: password);
                    
                    self.removeAllOverlays()
                    dispatch_async(dispatch_get_main_queue()) {
                        self.performSegueWithIdentifier("home", sender: self)
                    }
                }
                else{
                    
                    var usr: User = User();
                    usr.deleteData()
                    
                    let alert = UIAlertView()
                    alert.title = "Login error!"
                    alert.message = message
                    alert.addButtonWithTitle("ok")
                    alert.show()
                    self.removeAllOverlays()
                    
                }
            }
        }
        else
        {
            let alert = UIAlertView()
            alert.title = "Connection Error!"
            alert.message = "Failed to get data from the api please make sure you are connected to internet"
            alert.addButtonWithTitle("ok")
            alert.show()
            self.removeAllOverlays()
        }
        

    }

}

