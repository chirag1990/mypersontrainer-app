import UIKit
import Foundation
import CoreData

class User: NSObject {
    
    // Get user from DB
    func getUser()->NSArray{
        
        var appDel: AppDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate);
        var context:NSManagedObjectContext = appDel.managedObjectContext!
        
        var request = NSFetchRequest(entityName: "User")
        request.returnsObjectsAsFaults = false;
        request.predicate = NSPredicate(format: "id = %@", "1")
        
        var results:NSArray = context.executeFetchRequest(request, error: nil)!
        
        return results;
        
    }
    
    // Creates a new user
    func createNewUser(data: AnyObject, password: String){
        
        var appDel: AppDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate);
        var context:NSManagedObjectContext = appDel.managedObjectContext!
        
        var newUser = NSEntityDescription.insertNewObjectForEntityForName("User", inManagedObjectContext: context) as! NSManagedObject
        newUser.setValue(data["first_name"] as! String, forKey:"first_name");
        newUser.setValue(data["last_name"] as! String, forKey:"last_name");
        newUser.setValue(data["email"] as! String, forKey:"email");
        newUser.setValue("1", forKey:"id");
        newUser.setValue(data["id"] as! String, forKey:"user_id");
        newUser.setValue(password, forKey:"password");
        context.save(nil);
        
    }
    
    lazy var managedObjectContext : NSManagedObjectContext? = {
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        if let managedObjectContext = appDelegate.managedObjectContext {
            return managedObjectContext
        }
        else {
            return nil
        }
        }()
    
    // Empty Table
    func deleteData() {
        
        var appDel: AppDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate);
        var context:NSManagedObjectContext = appDel.managedObjectContext!
        
        let fetchRequest = NSFetchRequest(entityName: "User")
        fetchRequest.includesSubentities = false
        fetchRequest.returnsObjectsAsFaults = false
        
        fetchRequest.predicate = NSPredicate(format:"id == 1")
        
        var error: NSError?
        
        // moc is your NSManagedObjectContext here
        var items = context.executeFetchRequest(fetchRequest, error: &error)!
        
        for item in items {
            context.deleteObject(item as! NSManagedObject)
        }
    }
    
    
}